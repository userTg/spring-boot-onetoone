package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entities.Instructor;
import com.example.demo.entities.InstructorDetail;
import com.example.demo.repository.InstructorRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
class SpringBootOneToOneRestApplicationTest {
	
	@Autowired
	private InstructorRepository instructorRepository;
	
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetInstructors() {
		InstructorDetail id1 = new InstructorDetail("id1Channel", "sport");
		InstructorDetail id2 = new InstructorDetail("id2Channel", "lecture");
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com", id1);
		Instructor i2 = new Instructor("Luke", "Lucky", "ll@mail.com",id2);
		
		entityManager.persist(i1);
		entityManager.persist(i2);
		List<Instructor> allInstructorsFromDb = instructorRepository.findAll();
		List<Instructor> instructorList = new ArrayList<>();
		for (Instructor instructor : allInstructorsFromDb) {
			instructorList.add(instructor);
	}
	}

	@Test
	void testGetInstructorById() {
		InstructorDetail id1 = new InstructorDetail("id1Channel", "sport");
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com", id1);
		Instructor instructorSavedInDb = entityManager.persist(i1);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInDb.getId());
		assertEquals(instructorSavedInDb, instructorFromDb);
		assertThat(instructorFromDb.equals(instructorSavedInDb));
	}

	@Test
	void testCreateUser() {
		InstructorDetail id1 = new InstructorDetail("id1Channel", "sport");
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com", id1);
		//Instructor instructorSavedInDb = entityManager.persist(i1);
		Instructor instructorFromDb = instructorRepository.getOne(i1.getId());
		instructorFromDb.setLastName("Morris");
		assertThat(instructorFromDb.getLastName().equals(i1.getLastName()));
	}

	@Test
	void testUpdateUser() {
		InstructorDetail id1 = new InstructorDetail("id1Channel", "sport");
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com", id1);
		entityManager.persist(i1);
		Instructor getFromDb = instructorRepository.getOne(i1.getId());
		getFromDb.setLastName("admino");
		Instructor instructorFromDb = instructorRepository.getOne(i1.getId());
		instructorFromDb.setLastName("Morris");
		assertThat(getFromDb.getLastName().equals(i1.getLastName()));
		
	}

	@Test
	void testDeleteUser() {
		InstructorDetail id1 = new InstructorDetail("id1Channel", "sport");
		InstructorDetail id2 = new InstructorDetail("id2Channel", "lecture");
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com", id1);
		Instructor i2 = new Instructor("Luke", "Lucky", "ll@mail.com",id2);
		Instructor persist = entityManager.persist(i1);
		entityManager.persist(i2);
		entityManager.remove(persist);
		List<Instructor> allInstructorsFromDb = instructorRepository.findAll();
		List<Instructor> instructorList = new ArrayList<>();
		for (Instructor instructor : allInstructorsFromDb) {
			instructorList.add(instructor);
		}
		assertThat(instructorList.size()).isEqualTo(1);

	}
	}


